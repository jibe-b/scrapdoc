import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

import scrap_lowtechlab

def test_scrap_lowtechlab():
  expected = "http://lowtechlab.org/wiki/Explore"
  actual = "http://lowtechlab.org/wiki/Explore"

  assert(expected == actual)
